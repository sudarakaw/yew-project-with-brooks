use yew_project_with_brooks::App;

fn main() {
    yew::Renderer::<App>::new().render();
}
