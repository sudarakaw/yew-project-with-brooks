use std::ops::Deref;

use wasm_bindgen::JsCast;
use wasm_bindgen_futures::spawn_local;
use web_sys::{HtmlInputElement, InputEvent, SubmitEvent};
use yew::{function_component, html, use_state, Callback, Html, Properties};
use yew_router::prelude::{use_navigator, Link};
use yewdux::prelude::use_store;

use crate::{
    api::tasks::{self, TaskChanges, PRIORITIES},
    componenets::atoms::inputs::TextInput,
    router::Route,
    store::{update_task, AppStore},
};

#[derive(Properties, PartialEq)]
pub struct Props {
    pub id: u32,
}

#[function_component(EditTask)]
pub fn view(props: &Props) -> Html {
    let (store, dispatch) = use_store::<AppStore>();
    let task = store.get_task(props.id).unwrap_or_default();
    let history = use_navigator().unwrap();

    if task.id != props.id {
        history.push(&Route::Home);

        return html! {};
    }

    let state = use_state(TaskChanges::default);
    let token = store.token.clone();

    let onsubmit = {
        let state = state.clone();

        Callback::from(move |event: SubmitEvent| {
            event.prevent_default();

            let dispatch = dispatch.clone();
            let history = history.clone();
            let token = token.clone();

            let form = {
                let form = state.deref().clone();
                let completed_at = match form.completed_at {
                    Some(Some(_)) => Some(js_sys::Date::new_0().to_utc_string().as_string()),
                    x => x,
                };

                TaskChanges {
                    completed_at,
                    ..form
                }
            };

            // gloo::console::log!(serde_json::to_string_pretty(&form).unwrap());

            spawn_local(async move {
                tasks::update(&token, task.id, &form).await.unwrap();

                update_task(task.id, form, dispatch);

                history.push(&Route::Tasks { id: task.id });
            });
        })
    };

    let title_oninput = {
        let state = state.clone();

        Callback::from(move |title: String| {
            let form = state.deref().clone();
            let title = Some(title);

            state.set(TaskChanges { title, ..form });
        })
    };

    let description_oninput = {
        let state = state.clone();

        Callback::from(move |event: InputEvent| {
            let form = state.deref().clone();
            let description = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();
            let description = if description.is_empty() {
                None
            } else {
                Some(description)
            };

            state.set(TaskChanges {
                description,
                ..form
            });
        })
    };

    let priority_oninput = {
        let state = state.clone();

        Callback::from(move |event: InputEvent| {
            let form = state.deref().clone();
            let priority = Some(
                event
                    .target()
                    .unwrap()
                    .unchecked_into::<HtmlInputElement>()
                    .value()
                    .chars()
                    .next()
                    .unwrap(),
            );

            state.set(TaskChanges { priority, ..form });
        })
    };

    let completed_oninput = {
        let state = state.clone();

        Callback::from(move |event: InputEvent| {
            let form = state.deref().clone();
            let checked = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .checked();
            let completed_at = Some(if checked { Some("".to_owned()) } else { None });

            state.set(TaskChanges {
                completed_at,
                ..form
            });
        })
    };

    let form = state.deref().clone();
    let completed = form
        .completed_at
        .map(|v| v.is_some())
        .unwrap_or(task.completed_at.is_some());

    html! {
        <form class="container" {onsubmit}>
            <h2>{"Edit Todo Item"}</h2>

            <section class="d-flex align-items-start gap-2 w-100">
                <div class="w-100">
                    <TextInput
                        input_type="text"
                        id="editing-title"
                        label_text="Title"
                        value={form.title.unwrap_or(task.title)}
                        oninput={title_oninput}
                    />
                </div>

                <div class="d-flex gap-2">
                    <div class="badge pill border border-info text-info fs-6 align-self-stretch align-items-center d-flex">
                        <div class="form-check form-switch">
                            <input
                                type="checkbox"
                                class="form-check-input"
                                id="completed_checkbox"
                                data-test="completed"
                                checked={completed}
                                oninput={completed_oninput}
                            />
                            <label class="form-check-label pt-1" for="completed_checkbox">{"Completed"}</label>
                        </div>
                    </div>
                    <div class="badge pill border border-info text-info fs-6 align-self-stretch">
                        {"Priority: "}
                        <select class="form-control form-control-sm w-auto d-inline" data-test="editing-priority" oninput={priority_oninput}>
                        {
                            PRIORITIES.iter().map(|p| {
                                let selected = Some(*p) == form.priority.or(task.priority) ;

                                html! {
                                    <option value={p.to_string()} {selected}>{p}</option>
                                }
                            }).collect::<Html>()
                        }
                        </select>
                    </div>
                </div>
            </section>

            <div class="form-floating mt-2">
                <textarea
                    id="description"
                    class="form-control"
                    style="height: 7em"
                    data-test="editing-description"
                    placeholder=""
                    value={ form.description.or(task.description).unwrap_or_default()}
                    oninput={description_oninput}
                ></textarea>
                <label for="description">{"Description"}</label>
            </div>

            <div class="d-grid gap-2 d-sm-flex justify-content-sm-evenly mt-3">
                <button type="submit" class="btn btn-success btn-lg order-sm-1 flex-sm-fill" data-test="submit">{"Save"}</button>
                <div class="flex-sm-fill" data-test="cancel">
                    <Link<Route> to={Route::Tasks { id: task.id }} classes="btn btn-secondary btn-lg w-100">{"Cancel"}</Link<Route>>
                </div>
            </div>
        </form>
    }
}
