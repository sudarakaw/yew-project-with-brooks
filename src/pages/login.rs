use crate::{
    api::users,
    componenets::molecules::{AccountForm, FormData},
    router::Route,
    store::{login_reducer, AppStore},
};
use wasm_bindgen_futures::spawn_local;
use yew::{function_component, html, Callback, Html};
use yew_router::prelude::use_navigator;
use yewdux::prelude::use_store;

#[function_component(Login)]
pub fn view() -> Html {
    let (_, dispatch) = use_store::<AppStore>();
    let history = use_navigator().unwrap();

    let onsubmit = Callback::from(move |form: FormData| {
        let dispatch = dispatch.clone();
        let history = history.clone();

        spawn_local(async move {
            let user = users::login(form.username, form.password).await;

            login_reducer(user, dispatch);

            history.push(&Route::Home);
        });
    });

    html! {
        <AccountForm id={"login_form"} title={"Log In"} button_text={"Log In"} {onsubmit} />
    }
}
