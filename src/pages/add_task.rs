use std::ops::Deref;

use wasm_bindgen::JsCast;
use wasm_bindgen_futures::spawn_local;
use web_sys::{HtmlInputElement, InputEvent, SubmitEvent};
use yew::{function_component, html, use_state, Callback, Html};
use yew_router::prelude::{use_navigator, Link};
use yewdux::prelude::use_store;

use crate::{
    api::{
        tasks::{self, NewTask, PRIORITIES},
        ApiError,
    },
    componenets::atoms::inputs::TextInput,
    router::Route,
    store::{add_task, AppStore},
};

#[function_component(AddTask)]
pub fn view() -> Html {
    let (store, dispatch) = use_store::<AppStore>();
    let token = store.token.clone();
    let history = use_navigator().unwrap();

    if token.is_empty() {
        history.push(&Route::Home);

        return html! {};
    }

    let state = use_state(NewTask::default);

    let onsubmit = {
        let state = state.clone();

        Callback::from(move |event: SubmitEvent| {
            event.prevent_default();

            let dispatch = dispatch.clone();
            let history = history.clone();
            let token = token.clone();
            let task = state.deref().clone();

            // gloo::console::log!(serde_json::to_string_pretty(&task).unwrap());

            spawn_local(async move {
                match tasks::create(&token, &task).await {
                    Ok(task) => {
                        add_task(task, dispatch);
                    }
                    Err(ApiError::NotAuthenticated) => {}
                    Err(err) => {
                        gloo::console::error!(err.to_string());
                        panic!();
                    }
                };

                history.push(&Route::Home);
            });
        })
    };
    let title_oninput = {
        let state = state.clone();

        Callback::from(move |title: String| {
            let task = state.deref().clone();

            state.set(NewTask { title, ..task });
        })
    };

    let description_oninput = {
        let state = state.clone();

        Callback::from(move |event: InputEvent| {
            let task = state.deref().clone();
            let description = Some(
                event
                    .target()
                    .unwrap()
                    .unchecked_into::<HtmlInputElement>()
                    .value(),
            );

            state.set(NewTask {
                description,
                ..task
            });
        })
    };

    let priority_oninput = {
        let state = state.clone();

        Callback::from(move |event: InputEvent| {
            let task = state.deref().clone();
            let priority = Some(
                event
                    .target()
                    .unwrap()
                    .unchecked_into::<HtmlInputElement>()
                    .value()
                    .chars()
                    .next()
                    .unwrap(),
            );

            state.set(NewTask { priority, ..task });
        })
    };

    let task = state.deref().clone();

    html! {
        <form class="container" {onsubmit}>
            <h2>{"Add Todo Item"}</h2>

            <section class="d-flex align-items-start gap-2 w-100">
                <div class="w-100">
                    <TextInput
                        input_type="text"
                        id="title"
                        label_text="Title"
                        value={task.title}
                        oninput={title_oninput}
                    />
                </div>

                <div class="d-flex gap-2">
                    <div class="badge pill border border-info text-info fs-6 align-self-stretch">
                        {"Priority: "}
                        <select class="form-control form-control-sm w-auto d-inline" data-test="priority" oninput={priority_oninput}>
                        {
                            PRIORITIES.iter().map(|p| {
                                let selected = Some(*p) == task.priority;

                                html! {
                                    <option value={p.to_string()} {selected}>{p}</option>
                                }
                            }).collect::<Html>()
                        }
                        </select>
                    </div>
                </div>
            </section>

            <div class="form-floating mt-2">
                <textarea
                    id="description"
                    class="form-control"
                    style="height: 7em"
                    data-test="description"
                    placeholder=""
                    value={ task.description.unwrap_or_default()}
                    oninput={description_oninput}
                ></textarea>
                <label for="description">{"Description"}</label>
            </div>

            <div class="d-grid gap-2 d-sm-flex justify-content-sm-evenly mt-3">
                <button type="submit" class="btn btn-success btn-lg order-sm-1 flex-sm-fill" data-test="submit">{"Create"}</button>
                <div class="flex-sm-fill" data-test="cancel">
                    <Link<Route> to={Route::Home} classes="btn btn-secondary btn-lg w-100">{"Cancel"}</Link<Route>>
                </div>
            </div>
        </form>
    }
}
