use yew::{function_component, html, Html};
use yewdux::prelude::use_store;

use crate::{componenets::organisms::Tasks, store::AppStore};

#[function_component(Home)]
pub fn view() -> Html {
    let (store, _) = use_store::<AppStore>();

    html! {
        <div class="container">
            if !store.token.is_empty() {
            <Tasks tasks={store.tasks.clone()}/>
            }
        </div>
    }
}
