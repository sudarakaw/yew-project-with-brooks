use wasm_bindgen_futures::spawn_local;
use yew::{function_component, html, Callback, Html, Properties};
use yew_router::prelude::{use_navigator, Link};
use yewdux::prelude::use_store;

use crate::{
    api::tasks,
    router::Route,
    store::{self, remove_task, AppStore},
};

#[derive(Properties, PartialEq)]
pub struct Props {
    pub id: u32,
}

#[function_component(SingleTask)]
pub fn view(props: &Props) -> Html {
    let (store, dispatch) = use_store::<AppStore>();
    let history = use_navigator().unwrap();
    let token = store.token.clone();

    let task = store.get_task(props.id).unwrap_or_default();

    {
        let dispatch = dispatch.clone();

        if token.is_empty() {
            store::set_error_message("You must be logged in to view tasks", dispatch);
        }
    }

    if token.is_empty() || task.id != props.id {
        history.push(&Route::Home);

        return html! {};
    }

    let delete_onclick = Callback::from(move |_| {
        let history = history.clone();
        let dispatch = dispatch.clone();
        let token = token.clone();

        spawn_local(async move {
            tasks::delete(&token, task.id).await.unwrap();

            remove_task(task.id, dispatch);

            history.push(&Route::Home);
        });
    });

    html! {
        <div class="container">
            <h2 data-test="title">{ task.title.clone() }</h2>
            <section class="d-flex align-items-start gap-2 w-100">
                <span class="badge pill text-bg-info fs-6" data-test="completed">{"Completed: "}{task.completed_text()}</span>
                <span class="badge pill text-bg-info fs-6" data-test="priority">{"Priority: "}{task.priority.unwrap_or_default()}</span>

                <div class="ms-auto" data-test="edit">
                    <Link<Route> to={Route::EditTasks { id: task.id }} classes="btn btn-outline-info">{"Edit"}</Link<Route>>
                </div>
                <button type="button" class="btn btn-outline-danger" data-test="delete" onclick={delete_onclick}>{"Delete"}</button>
            </section>
            <hr />
            <p data-test="description">{ task.description.unwrap_or_default() }</p>
        </div>
    }
}
