use wasm_bindgen::JsCast;
use web_sys::HtmlInputElement;
use yew::{function_component, html, Callback, Html, InputEvent, Properties};

#[derive(Properties, PartialEq)]
pub struct Props {
    pub id: String,
    pub input_type: String,
    pub label_text: Option<String>,
    pub oninput: Callback<String>,
    pub value: Option<String>,
}

#[function_component(TextInput)]
pub fn view(props: &Props) -> Html {
    let id = props.id.clone();
    let label_text = props.label_text.clone().unwrap_or_else(|| id.clone());

    let oninput = {
        let emit_oninput = props.oninput.clone();

        Callback::from(move |event: InputEvent| {
            event.prevent_default();

            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();

            emit_oninput.emit(value);
        })
    };

    html! {
        <div class="form-floating">
            <input type={props.input_type.clone()} class="form-control" id={id.clone()} placeholder="" value={props.value.clone().unwrap_or_default()} data-test={id.clone()} {oninput} />
            <label for={id}>{label_text}</label>
        </div>
    }
}
