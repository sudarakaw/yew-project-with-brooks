use wasm_bindgen::JsCast;
use wasm_bindgen_futures::spawn_local;
use web_sys::{HtmlInputElement, InputEvent};
use yew::{function_component, html, Callback, Html, Properties};
use yew_router::prelude::Link;
use yewdux::prelude::use_store;

use crate::{
    api::tasks::{self, Task, TaskChanges, TaskFilter, TaskSort},
    router::Route,
    store::{update_filter, update_sort, update_task, AppStore},
};

#[derive(Properties, PartialEq)]
pub struct Props {
    pub tasks: Vec<Task>,
}

#[function_component(Tasks)]
pub fn view(props: &Props) -> Html {
    let (store, dispatch) = use_store::<AppStore>();

    let completed_oninput = {
        let store = store.clone();
        let dispatch = dispatch.clone();

        Callback::from(move |event: InputEvent| {
            let input = event.target().unwrap().unchecked_into::<HtmlInputElement>();
            let task_id = input.value().parse::<u32>().unwrap_or(0);

            if 1 > task_id {
                input.set_checked(!input.checked());

                return;
            }

            let token = store.token.clone();
            let dispatch = dispatch.clone();

            let task = TaskChanges {
                completed_at: if input.checked() {
                    Some(js_sys::Date::new_0().to_utc_string().as_string())
                } else {
                    Some(None)
                },
                ..TaskChanges::default()
            };

            // gloo::console::log!(serde_json::to_string_pretty(&task).unwrap());

            spawn_local(async move {
                tasks::update(&token, task_id, &task).await.unwrap();

                update_task(task_id, task, dispatch);
            });
        })
    };

    let filter_oninput = {
        let dispatch = dispatch.clone();

        Callback::from(move |event: InputEvent| {
            let dispatch = dispatch.clone();
            let selection = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();
            let filter_option = serde_json::from_str(&selection).unwrap_or_default();

            update_filter(filter_option, dispatch);
        })
    };

    let sort_oninput = Callback::from(move |event: InputEvent| {
        let dispatch = dispatch.clone();
        let selection = event
            .target()
            .unwrap()
            .unchecked_into::<HtmlInputElement>()
            .value();
        let sort_option = serde_json::from_str(&selection).unwrap_or_default();

        update_sort(sort_option, dispatch);
    });

    // Apply filter
    let mut visible_tasks = props
        .tasks
        .clone()
        .into_iter()
        .filter(|t| match store.filter_option {
            TaskFilter::All => true,
            TaskFilter::Pending => t.completed_at.is_none(),
            TaskFilter::Complete => t.completed_at.is_some(),
            TaskFilter::Priority(p) => Some(p) == t.priority,
        })
        .collect::<Vec<Task>>();

    // Apply sorting
    visible_tasks.sort_by(|a, b| match &store.sort_option {
        TaskSort::Created => a.id.cmp(&b.id),
        TaskSort::Priority => a.priority.cmp(&b.priority),
        TaskSort::Title => a.title.cmp(&b.title),
    });

    html! {
        <>
            <form>
                <div class="row align-items-center">
                    <div class="col-sm-4"></div>
                    <lable for="" class="col-form-label col-sm-1 text-end">{"Sort: "}</lable>
                    <div class="col-sm-3">
                        <select class="form-select" data-test="sort" oninput={sort_oninput}>
                            {
                                TaskSort::options().iter().map(|(sort, text)| {
                                    let selected = sort == &store.sort_option;

                                    html! {
                                        <option value={serde_json::to_string(sort).unwrap()} {selected}>{text}</option>
                                    }
                                }).collect::<Html>()
                            }
                        </select>
                    </div>
                    <lable for="" class="col-form-label col-sm-1 text-end">{"Filter: "}</lable>
                    <div class="col-sm-3">
                        <select class="form-select" data-test="filter" oninput={filter_oninput}>
                           {
                                TaskFilter::options().iter().map(|(filter, text)| {
                                    let selected = filter == &store.filter_option;

                                    html! {
                                        <option value={serde_json::to_string(filter).unwrap()} {selected}>{text}</option>
                                    }
                                }).collect::<Html>()
                            }
                        </select>
                    </div>
                </div>
            </form>

            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{"Priority"}</th>
                        <th class="text-center">{"Completed"}</th>
                        <th>{"Task"}</th>
                    </tr>
                </thead>
                <tbody class="table-group-divider">
                {
                    visible_tasks.iter().map( |t| {
                        html! {
                        <tr>
                            <td class={format!("text-center {}", t.priority_color())} data-test="priority">{t.priority.unwrap_or('?')}</td>
                            <td class="text-center"><input type="checkbox" value={t.id.to_string()} checked={t.completed_at.is_some()} data-test="completed" oninput={completed_oninput.clone()} /></td>
                            <td data-test="tasklink"><Link<Route> to={Route::Tasks { id: t.id }}>{t.title.clone()}</Link<Route>></td>
                        </tr>
                        }
                    }).collect::<Html>()
                }
                </tbody>
            </table>
        </>
    }
}
