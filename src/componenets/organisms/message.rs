use yew::{function_component, html, use_effect, use_state, Html};
use yewdux::prelude::use_store;

use crate::store::{self, AppStore};

#[function_component(Message)]
pub fn view() -> Html {
    let (store, dispatch) = use_store::<AppStore>();
    let timer_id = use_state(|| None);

    {
        let store = store.clone();

        use_effect(move || {
            if !store.error_message.is_empty() && timer_id.is_none() {
                let id = {
                    let timer_id = timer_id.clone();

                    gloo::timers::callback::Timeout::new(10000, move || {
                        store::clear_error_message(dispatch);

                        timer_id.set(None);
                    })
                    .forget()
                };

                timer_id.set(Some(id));
            }

            || {}
        });
    }

    let display_class = if store.error_message.is_empty() {
        "d-none"
    } else {
        ""
    };

    html! {
        <div class={format!("container {display_class}")}>
            <p class="alert alert-danger text-center" data-test="error">{&store.error_message}</p>
        </div>
    }
}
