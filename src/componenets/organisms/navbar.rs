use std::ops::Deref;

use wasm_bindgen_futures::spawn_local;
use yew::{function_component, html, Callback, Html};
use yew_router::prelude::{use_navigator, Link};
use yewdux::prelude::use_store;

use crate::{
    api::users,
    router::Route,
    store::{self, AppStore},
};

#[function_component(NavBar)]
pub fn view() -> Html {
    let (store, dispatch) = use_store::<AppStore>();
    let store = store.deref().clone();
    let token = store.token.clone();
    let history = use_navigator().unwrap();

    let logout_onclick = Callback::from(move |_| {
        let history = history.clone();
        let dispatch = dispatch.clone();
        let token = token.clone();

        spawn_local(async move {
            match users::logout(&token).await {
                Ok(_) => {
                    store::logout(dispatch.clone());
                    store::clear_error_message(dispatch);

                    history.push(&Route::Home);
                }
                Err(error) => gloo::console::error!(error.to_string()),
            }
        });
    });

    html! {
        <nav class="navbar navbar-expand-md mb-4 bg-body-tertiary">
            <div class="container-fluid">
                <div data-test="logo">
                    <Link<Route> to={Route::Home} classes="navbar-brand fs-1 h1">{"Todo App"}</Link<Route>>
                </div>

                if !store.token.is_empty() {
                <span class="navbar-text fs-5 p-0" data-test="welcome">{"Welcome, "}{store.username}</span>
                }

                <ul class="nav">
                    if store.token.is_empty() {
                    <li class="nav-item">
                        <Link<Route> to={Route::CreateAccount} classes="btn btn-warning fs-5">{"Create Account"}</Link<Route>>
                    </li>
                    <li class="nav-item">
                        <Link<Route> to={Route::Login} classes="btn btn-outline-success fs-5">{"Log In"}</Link<Route>>
                    </li>
                    }
                else {
                    <li class="nav-item" data-test="add-task">
                        <Link<Route> to={Route::AddTasks} classes="btn btn-outline-success fs-5">{"Add Task"}</Link<Route>>
                    </li>
                    <li class="nav-item">
                        <button type="button" class="btn btn-outline-warning fs-5" onclick={logout_onclick}>{"Log Out"}</button>
                    </li>
                }
                </ul>
            </div>
        </nav>
    }
}
