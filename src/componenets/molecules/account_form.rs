use std::ops::Deref;

use web_sys::SubmitEvent;
use yew::{function_component, html, use_state, Callback, Html, Properties};

use crate::componenets::atoms::inputs::TextInput;

#[derive(Default, Clone)]
pub struct FormData {
    pub username: String,
    pub password: String,
}

#[derive(Properties, PartialEq)]
pub struct Props {
    pub id: String,
    pub title: String,
    pub button_text: String,
    pub onsubmit: Callback<FormData>,
}

#[function_component(AccountForm)]
pub fn view(props: &Props) -> Html {
    let state = use_state(FormData::default);

    let onsubmit = {
        let state = state.clone();
        let emit_onsubmit = props.onsubmit.clone();

        Callback::from(move |event: SubmitEvent| {
            event.prevent_default();

            let form_data = state.deref().clone();

            emit_onsubmit.emit(form_data);
        })
    };

    let handle_username_input = {
        let state = state.clone();

        Callback::from(move |username| {
            let form_data = state.deref().clone();

            state.set(FormData {
                username,
                ..form_data
            });
        })
    };

    let handle_password_input = {
        let state = state.clone();

        Callback::from(move |password| {
            let form_data = state.deref().clone();

            state.set(FormData {
                password,
                ..form_data
            });
        })
    };

    html! {
        <form class="account-form m-auto text-center" id={props.id.clone()} {onsubmit}>
            <h3 class="mb-3 fw-normal">{props.title.clone()}</h3>

            <TextInput input_type="text" id="username" label_text="Username" value={state.username.clone()} oninput={handle_username_input} />
            <TextInput input_type="password" id="password" label_text="Password" value={state.password.clone()} oninput={handle_password_input} />

            <button type="submit" class="btn btn-lg btn-success mt-3 w-100" data-test="submit">{props.button_text.clone()}</button>
        </form>
    }
}
