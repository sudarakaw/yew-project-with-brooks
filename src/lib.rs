mod api;
mod componenets;
mod pages;
mod router;
mod store;

use api::{tasks, ApiError};
use componenets::organisms::{Message, NavBar};
use router::{switch, Route};
use store::AppStore;
use wasm_bindgen_futures::spawn_local;
use yew::{function_component, html, use_effect, Html};
use yew_router::{BrowserRouter, Switch};
use yewdux::prelude::use_store;

#[function_component(App)]
pub fn app() -> Html {
    let (store, dispatch) = use_store::<AppStore>();
    let token = store.token.clone();

    use_effect(move || {
        if !token.is_empty() {
            spawn_local(async move {
                match tasks::get_all(&token).await {
                    Ok(tasks) => {
                        store::set_tasks(tasks, dispatch);
                    }
                    Err(ApiError::NotAuthenticated) => {
                        store::logout(dispatch);
                    }
                    Err(err) => gloo::console::error!(err.to_string()),
                };
            });
        }

        || {}
    });

    html! {
        <BrowserRouter>
            <header>
                <NavBar />
                <Message />
            </header>

            <Switch<Route> render={switch} />
        </BrowserRouter>
    }
}
