use thiserror::Error;

#[derive(Debug, Error)]
pub enum ApiError {
    #[error("Expired or missing authentication token")]
    NotAuthenticated,
    #[error("Unknown Network error")]
    Unknown,
}

impl From<u16> for ApiError {
    fn from(status: u16) -> Self {
        match status {
            401 => ApiError::NotAuthenticated,
            _ => ApiError::Unknown,
        }
    }
}
