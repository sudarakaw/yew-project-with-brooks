use reqwasm::http::Request;
use serde::{Deserialize, Serialize};
use serde_json::json;

use super::{ApiError, ApiResponse, BASE_URI};

#[derive(Serialize, Deserialize)]
pub struct User {
    id: u32,
    pub username: String,
    pub token: String,
}

pub async fn create(username: String, password: String) -> User {
    let body = json!({
        "username": username,
        "password": password
    });

    Request::post(&format!("{BASE_URI}/users"))
        .header("content-type", "application/json")
        .body(body.to_string())
        .send()
        .await
        .unwrap()
        .json::<ApiResponse<User>>()
        .await
        .unwrap()
        .data
}

pub async fn login(username: String, password: String) -> User {
    let body = json!({
        "username": username,
        "password": password
    });

    Request::post(&format!("{BASE_URI}/users/login"))
        .header("content-type", "application/json")
        .body(body.to_string())
        .send()
        .await
        .unwrap()
        .json::<ApiResponse<User>>()
        .await
        .unwrap()
        .data
}

pub async fn logout(token: &str) -> Result<(), ApiError> {
    let response = Request::post(&format!("{BASE_URI}/users/logout"))
        .header("x-auth-token", token)
        .send()
        .await
        .unwrap();
    if response.ok() {
        Ok(())
    } else {
        Err(response.status().into())
    }
}
