use reqwasm::http::Request;
use serde::{Deserialize, Serialize};

use super::{ApiError, ApiResponse, BASE_URI};

pub const PRIORITIES: [char; 3] = ['A', 'B', 'C'];

#[derive(Default, Deserialize, Serialize, Clone, PartialEq)]
pub struct Task {
    pub completed_at: Option<String>,
    pub description: Option<String>,
    pub id: u32,
    pub priority: Option<char>,
    pub title: String,
}

impl Task {
    pub fn completed_text(&self) -> String {
        if self.completed_at.is_none() {
            "X".to_owned()
        } else {
            "✓".to_owned()
        }
    }

    pub fn priority_color(&self) -> String {
        match self.priority {
            Some('A') => "text-light".to_owned(),
            Some('B') => "text-warning".to_owned(),
            Some('C') => "text-danger".to_owned(),
            _ => "text-secondary".to_owned(),
        }
    }
}

#[derive(Default, Clone, PartialEq, Serialize, Deserialize)]
pub enum TaskFilter {
    #[default]
    All,
    Pending,
    Complete,
    Priority(char),
}

impl TaskFilter {
    pub fn options() -> Vec<(TaskFilter, String)> {
        vec![
            (TaskFilter::All, "None".to_owned()),
            (TaskFilter::Pending, "Uncompleted".to_owned()),
            (TaskFilter::Complete, "Completed".to_owned()),
            (TaskFilter::Priority('A'), "Priority A".to_owned()),
            (TaskFilter::Priority('B'), "Priority B".to_owned()),
            (TaskFilter::Priority('C'), "Priority C".to_owned()),
        ]
    }
}

#[derive(Default, Clone, PartialEq, Serialize, Deserialize)]
pub enum TaskSort {
    #[default]
    Created,
    Title,
    Priority,
}

impl TaskSort {
    pub fn options() -> Vec<(TaskSort, String)> {
        vec![
            (TaskSort::Created, "Created Order".to_owned()),
            (TaskSort::Priority, "priority".to_owned()),
            (TaskSort::Title, "Name".to_owned()),
        ]
    }
}

pub async fn get_all(token: &str) -> Result<Vec<Task>, ApiError> {
    let response = Request::get(&format!("{BASE_URI}/tasks"))
        .header("x-auth-token", token)
        .send()
        .await
        .unwrap();

    if response.ok() {
        Ok(response
            .json::<ApiResponse<Vec<Task>>>()
            .await
            .unwrap()
            .data)
    } else {
        Err(response.status().into())
    }
}

#[derive(Default, Clone, Serialize)]
pub struct TaskChanges {
    // NOTE: Using nested Option for `completed_at` because we need to represent
    // checked/unchecked status with changed/unchanged status in the task edit
    // form.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub completed_at: Option<Option<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub priority: Option<char>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
}

pub async fn update(token: &str, task_id: u32, changes: &TaskChanges) -> Result<(), ApiError> {
    let response = Request::patch(&format!("{BASE_URI}/tasks/{task_id}"))
        .header("x-auth-token", token)
        .header("content-type", "application/json")
        .body(serde_json::to_string(changes).unwrap())
        .send()
        .await
        .unwrap();

    if response.ok() {
        Ok(())
    } else {
        Err(response.status().into())
    }
}

pub async fn delete(token: &str, task_id: u32) -> Result<(), ApiError> {
    let response = Request::delete(&format!("{BASE_URI}/tasks/{task_id}"))
        .header("x-auth-token", token)
        .send()
        .await
        .unwrap();

    if response.ok() {
        Ok(())
    } else {
        Err(response.status().into())
    }
}

#[derive(Serialize, Clone)]
pub struct NewTask {
    pub description: Option<String>,
    pub priority: Option<char>,
    pub title: String,
}

impl Default for NewTask {
    fn default() -> Self {
        Self {
            description: None,
            priority: Some('A'),
            title: String::default(),
        }
    }
}

pub async fn create(token: &str, task: &NewTask) -> Result<Task, ApiError> {
    let response = Request::post(&format!("{BASE_URI}/tasks"))
        .header("x-auth-token", token)
        .header("content-type", "application/json")
        .body(serde_json::to_string(task).unwrap())
        .send()
        .await
        .unwrap();

    if response.ok() {
        let task = response.json::<ApiResponse<Task>>().await.unwrap().data;

        Ok(task)
    } else {
        Err(response.status().into())
    }
}
