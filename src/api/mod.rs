use serde::Deserialize;

pub mod tasks;
pub mod users;

pub mod error;
pub use error::*;

#[derive(Deserialize)]
struct ApiResponse<T> {
    data: T,
}

const BASE_URI: &str = include_str!("./api_base_uri.txt");
