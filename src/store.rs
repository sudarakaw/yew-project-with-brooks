use serde::{Deserialize, Serialize};
use yewdux::{prelude::Dispatch, store::Store};

use crate::api::{
    tasks::{Task, TaskChanges, TaskFilter, TaskSort},
    users::User,
};

#[derive(Store, PartialEq, Default, Clone, Serialize, Deserialize)]
#[store(storage = "local")]
pub struct AppStore {
    pub username: String,
    pub token: String,
    pub tasks: Vec<Task>,
    pub filter_option: TaskFilter,
    pub sort_option: TaskSort,
    pub error_message: String,
}

impl AppStore {
    pub fn get_task(&self, id: u32) -> Option<Task> {
        self.tasks.iter().find(|t| t.id == id).cloned()
    }
}

pub fn login_reducer(user: User, dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(move |store| {
        store.username = user.username;
        store.token = user.token;
    });
}

pub fn set_tasks(tasks: Vec<Task>, dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(move |store| {
        store.tasks = tasks;
    });
}

pub fn logout(dispatch: Dispatch<AppStore>) {
    dispatch.reduce(move |_| AppStore::default().into());
}

pub fn update_task(task_id: u32, changes: TaskChanges, dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(move |store| {
        let task = store.tasks.iter_mut().find(|t| t.id == task_id);
        let task = if let Some(task) = task {
            task
        } else {
            gloo::console::error!("Updated task not found in local list.");
            panic!();
        };

        if let Some(title) = changes.title {
            task.title = title;
        }

        if let Some(completed_at) = changes.completed_at {
            task.completed_at = completed_at;
        }

        if changes.priority.is_some() {
            task.priority = changes.priority;
        }

        if changes.description.is_some() {
            task.description = changes.description;
        }
    });
}

pub fn remove_task(task_id: u32, dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(|store| {
        store.tasks.retain(|t| t.id != task_id);
    });
}

pub fn add_task(task: Task, dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(|store| {
        store.tasks.insert(0, task);
    });
}

pub fn update_filter(filter_option: TaskFilter, dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(|store| {
        store.filter_option = filter_option;
    });
}

pub fn update_sort(sort_option: TaskSort, dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(|store| {
        store.sort_option = sort_option;
    });
}

pub fn clear_error_message(dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(|store| {
        store.error_message = String::new();
    });
}

pub fn set_error_message(message: &str, dispatch: Dispatch<AppStore>) {
    dispatch.reduce_mut(|store| {
        store.error_message = message.to_owned();
    });
}
