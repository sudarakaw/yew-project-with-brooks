use yew::{html, Html};
use yew_router::Routable;

use crate::pages::{AddTask, CreateAccount, EditTask, Home, Login, SingleTask};

#[derive(PartialEq, Routable, Clone)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/create-account")]
    CreateAccount,
    #[at("/login")]
    Login,
    #[at("/tasks")]
    AddTasks,
    #[at("/tasks/:id")]
    Tasks { id: u32 },
    #[at("/tasks/:id/edit")]
    EditTasks { id: u32 },
}

pub fn switch(route: Route) -> Html {
    match route {
        Route::Home => html! { <Home /> },
        Route::CreateAccount => html! { <CreateAccount /> },
        Route::Login => html! { <Login /> },
        Route::AddTasks => html! { <AddTask /> },
        Route::Tasks { id } => html! { <SingleTask id={id} /> },
        Route::EditTasks { id } => html! { <EditTask id={id} /> },
    }
}
